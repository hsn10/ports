FreeBSD Ports repository
========================

_(c) Radim Kolar 2014, 2018, 2020_

This repository contains Makefiles for compilling ports.

### What is here:

| Directory   |                Content                                       |
| ----------- |--------------------------------------------------------------|
| maintained/ | Ports I officialy maintain in FreeBSD ports tree.            |
| hacked/     | Private modifications to offical ports.                      |
| myports/    | Private ports not submitted to FreeBSD ports or rejected.    |
| historic/   | Upstream do not exists anymore.                              |
| expired/    | Ports removed from FreeBSD collection.                       |

